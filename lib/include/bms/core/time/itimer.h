#pragma once

#include <chrono>
#include <cstdint>
#include <memory>

#include "bms/core/event/ievent.h"

namespace bms {
class ITimer {
 public:
  virtual ~ITimer() = default;

  virtual uint64_t single(std::unique_ptr<const IEvent> upEvent,
                          std::chrono::milliseconds ms) = 0;
  virtual uint64_t repeat(std::unique_ptr<const IEvent> upEvent,
                          std::chrono::milliseconds ms) = 0;
  virtual void cancel(uint64_t handle) = 0;
};
}  // namespace bms
