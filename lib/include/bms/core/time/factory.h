#pragma once

#include <memory>

#include "bms/core/time/itimer.h"

namespace bms {
class IEventLoop;
std::unique_ptr<ITimer> createTimer(IEventLoop& eventLoop);
}  // namespace bms