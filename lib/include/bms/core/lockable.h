#pragma once

#include <mutex>

namespace bms {
template <typename T, typename LockT = std::mutex>
struct Lockable {
  mutable LockT m_mutex;
  T m_t;
};
}  // namespace bms