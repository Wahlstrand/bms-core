#pragma once

#include <memory>

#include "bms/core/event/ievent_bus.h"
#include "bms/core/event/ievent_loop.h"

namespace bms {
std::shared_ptr<IEventBus> createEventBus();
std::unique_ptr<IEventLoop> createEventLoop(
    std::shared_ptr<IEventBus> spEventBus);
}  // namespace bms