#pragma once

#include <memory>

#include "bms/core/event/ievent.h"

namespace bms {
template <typename TagT, typename ValueT = void>
struct Event : public IEvent {
  Event(const ValueT& val) : m_val(val) {}
  std::unique_ptr<IEvent> clone() const override {
    return std::unique_ptr<IEvent>(new Event<TagT, ValueT>(*this));
  }
  ValueT m_val;
};

template <typename TagT>
struct Event<TagT, void> : public IEvent {
  std::unique_ptr<IEvent> clone() const override {
    return std::unique_ptr<IEvent>(new Event<TagT, void>());
  }
};

}  // namespace bms