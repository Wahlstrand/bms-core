#pragma once

#include <memory>

#include "bms/core/event/ievent.h"

namespace bms {
class IEventHandler;
class ITimer;
class IEventLoop {
 public:
  virtual ~IEventLoop() = default;

  virtual void dispatch(std::unique_ptr<const IEvent> upEvent) = 0;
  virtual void publish(std::unique_ptr<const IEvent> upEvent) = 0;
  virtual ITimer& timer() const = 0;
  virtual void add(IEventHandler* pEventHandler) = 0;
  virtual void remove(IEventHandler* pEventHandler) = 0;
};
}  // namespace bms
