#pragma once

#include <memory>
#include <vector>

#include "bms/core/event/ievent.h"

namespace bms {
class IEventHandler;
class IEventBusListener {
 public:
  virtual ~IEventBusListener() = default;

  // Called from any thread
  virtual void fromBus(std::vector<std::unique_ptr<const IEvent>> events) = 0;
};

class IEventBus {
 public:
  virtual ~IEventBus() = default;

  // Called from any thread
  virtual void push(std::vector<std::unique_ptr<const IEvent>> event) = 0;
  virtual void add(IEventBusListener* pListener) = 0;
  virtual void remove(IEventBusListener* pListener) = 0;
};

}  // namespace bms
