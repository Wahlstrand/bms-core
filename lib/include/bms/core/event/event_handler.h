#pragma once

#include <functional>

#include "bms/core/event/ievent_handler.h"
#include "bms/core/event/ievent_loop.h"

// NOTE: If this class is created from another thread than the one
// associated with the event loop, make sure that all setup is done so that the
// callback can be called.
//
// E.g, this could cause a problem:
//
// struct MyStruct {
//   MyStruct(IEventLoop &eventLoop)
//       : m_handler(eventLoop, []() { std::cout << m_someValue << '\n'; }),
//         m_someValue(4.0) {}
//
//   EventHandler m_handler;
//   double m_someValue;
// };
//
// The callback could fire before m_someValue is initialized, causing access to
// m_someValue from the even loop thread which may result in either accessing an
// uninitialized value or simultaneous read/write from different threads.
//
// The correct way would be to reverse the initialization order so that
// m_someValue is ready for use when the event handler is registered. That way
// we guarantee a safe handover to the even loop thread:
//
// struct MyStruct {
//   MyStruct(IEventLoop &eventLoop)
//       : m_someValue(4.0),
//         m_handler(eventLoop, []() { std::cout << m_someValue << '\n'; }), {}
//
//   double m_someValue;
//   EventHandler m_handler;
// };
namespace bms {

template <typename EventT>
class EventHandler final : public bms::IEventHandler {
 public:
  EventHandler(IEventLoop &eventLoop,
               std::function<void(const EventT &)> callback)
      : m_eventLoop(eventLoop), m_callback(std::move(callback)) {
    m_eventLoop.add(this);
  }
  ~EventHandler() { m_eventLoop.remove(this); }

  // IEventHandler
  void onEvent(const IEvent &ev) override {
    if (const auto pMyEvent = dynamic_cast<const EventT *>(&ev)) {
      m_callback(*pMyEvent);
    }
  }

 private:
  IEventLoop &m_eventLoop;
  std::function<void(const EventT &)> m_callback;
};

}  // namespace bms
