#pragma once

namespace bms {
struct IEvent;
class IEventHandler {
 public:
  virtual ~IEventHandler() = default;
  virtual void onEvent(IEvent const &ev) = 0;
};
}  // namespace bms
