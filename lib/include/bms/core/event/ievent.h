#pragma once

#include <memory>

namespace bms {
struct IEvent {
  virtual ~IEvent() = default;
  virtual std::unique_ptr<IEvent> clone() const = 0;
};
}  // namespace bms