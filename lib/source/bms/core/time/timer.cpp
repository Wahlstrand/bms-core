#include "bms/core/time/timer.h"

#include <algorithm>

#include "bms/core/event/ievent_loop.h"

bms::Timer::Timer(IEventLoop& eventLoop)
    : m_eventLoop(eventLoop),
      m_queue(),
      m_mutex(),
      m_stop(false),
      m_modified(false),
      m_cv(),
      m_nextHandle(1u),
      m_thread([this]() { run(); }) {}

bms::Timer::~Timer() {
  {
    std::unique_lock lock(m_mutex);
    m_stop = true;
  }
  m_cv.notify_one();
  m_thread.join();
}

// ITimer
uint64_t bms::Timer::single(std::unique_ptr<const IEvent> upEvent,
                            std::chrono::milliseconds timeout) {
  const uint64_t handle = m_nextHandle++;
  {
    using namespace std::chrono_literals;
    std::unique_lock lock(m_mutex);
    m_queue.emplace_back(std::chrono::high_resolution_clock::now() + timeout,
                         0ms, handle, std::move(upEvent));
    std::sort(m_queue.begin(), m_queue.end(), std::greater<ScheduledEvent>());
    m_modified = true;
  }
  m_cv.notify_one();
  return handle;
}
uint64_t bms::Timer::repeat(std::unique_ptr<const IEvent> upEvent,
                            std::chrono::milliseconds interval) {
  if (interval.count() == 0) {
    throw std::runtime_error("invalid interval");
  }

  const uint64_t handle = m_nextHandle++;
  {
    std::unique_lock lock(m_mutex);
    m_queue.emplace_back(std::chrono::high_resolution_clock::now() + interval,
                         interval, handle, std::move(upEvent));
    std::sort(m_queue.begin(), m_queue.end(), std::greater<ScheduledEvent>());
    m_modified = true;
  }
  m_cv.notify_one();
  return handle;
}
void bms::Timer::cancel(uint64_t handle) {
  {
    std::unique_lock lock(m_mutex);
    m_queue.erase(
        std::remove_if(m_queue.begin(), m_queue.end(),
                       [handle](const ScheduledEvent& scheduledEvent) {
                         return scheduledEvent.m_handle == handle;
                       }),
        m_queue.end());
  }
  m_cv.notify_one();
}
//
void bms::Timer::run() {
  using namespace std::chrono_literals;
  while (true) {
    std::unique_lock lock(m_mutex);
    auto wakeup = m_queue.empty()
                      ? std::chrono::high_resolution_clock::now() + 10000ms
                      : m_queue.back().m_timeout;
    if (m_cv.wait_until(lock, wakeup, [this]() { return hasWork(); })) {
      if (m_stop) {
        return;
      }
      process();
      m_modified = false;
    }
  }
}

bool bms::Timer::hasWork() const {
  return m_stop || m_modified ||
         (!m_queue.empty() && std::chrono::high_resolution_clock::now() >=
                                  m_queue.back().m_timeout);
}
void bms::Timer::process() {
  while (!m_queue.empty() && std::chrono::high_resolution_clock::now() >=
                                 m_queue.back().m_timeout) {
    auto scheduledEvent = std::move(m_queue.back());
    m_queue.pop_back();
    m_eventLoop.dispatch(scheduledEvent.m_upEvent->clone());
    if (scheduledEvent.m_interval.count() != 0u) {
      scheduledEvent.m_timeout += scheduledEvent.m_interval;
      m_queue.push_back(std::move(scheduledEvent));
      std::sort(m_queue.begin(), m_queue.end(), std::greater<ScheduledEvent>());
    }
  }
}
