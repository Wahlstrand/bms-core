#pragma once

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <thread>
#include <vector>

#include "bms/core/lockable.h"
#include "bms/core/time/itimer.h"

namespace bms {
class IEventLoop;
class Timer final : public ITimer {
 public:
  Timer(IEventLoop& eventLoop);
  ~Timer();

  // ITimer
  uint64_t single(std::unique_ptr<const IEvent> upEvent,
                  std::chrono::milliseconds timeout) final;
  uint64_t repeat(std::unique_ptr<const IEvent> upEvent,
                  std::chrono::milliseconds interval) final;
  void cancel(uint64_t handle) final;

 private:
  struct ScheduledEvent {
    ScheduledEvent(
        std::chrono::time_point<std::chrono::high_resolution_clock> timeout,
        std::chrono::milliseconds interval, uint32_t handle,
        std::unique_ptr<const IEvent> upEvent)
        : m_timeout(timeout),
          m_interval(interval),
          m_handle(handle),
          m_upEvent(std::move(upEvent)) {}

    bool operator>(const ScheduledEvent& other) const {
      return m_timeout > other.m_timeout;
    }

    std::chrono::time_point<std::chrono::high_resolution_clock> m_timeout;
    std::chrono::milliseconds m_interval;
    uint32_t m_handle;
    std::unique_ptr<const IEvent> m_upEvent;
  };

  IEventLoop& m_eventLoop;
  std::vector<ScheduledEvent> m_queue;
  std::mutex m_mutex;
  bool m_stop;
  bool m_modified;
  std::condition_variable m_cv;
  std::atomic<uint64_t> m_nextHandle;

  std::thread m_thread;

  //
  void run();

  // These assume that lock is held
  bool hasWork() const;
  void process();
};

}  // namespace bms