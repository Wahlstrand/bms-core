#include "bms/core/time/factory.h"

#include "bms/core/time/timer.h"

std::unique_ptr<bms::ITimer> bms::createTimer(IEventLoop& eventLoop) {
  return std::make_unique<Timer>(eventLoop);
}