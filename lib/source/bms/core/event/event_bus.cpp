#include "bms/core/event/event_bus.h"

#include <algorithm>
#include <iterator>

bms::EventBus::EventBus()
    : m_listeners(), m_cv(), m_stop(false), m_thread([this]() { run(); }) {}

bms::EventBus::~EventBus() {
  m_stop = true;
  m_cv.m_t.notify_one();
  m_thread.join();
}

void bms::EventBus::push(std::vector<std::unique_ptr<const IEvent>> events) {
  {
    std::unique_lock lock(m_eventQueue.m_mutex);
    m_eventQueue.m_t.insert(m_eventQueue.m_t.end(),
                            std::make_move_iterator(events.begin()),
                            std::make_move_iterator(events.end()));
  }
  m_cv.m_t.notify_one();
}
void bms::EventBus::add(IEventBusListener *pListener) {
  std::unique_lock lock(m_listeners.m_mutex);
  m_listeners.m_t.push_back(pListener);
}
void bms::EventBus::remove(IEventBusListener *pListener) {
  std::unique_lock lock(m_listeners.m_mutex);
  m_listeners.m_t.erase(
      std::remove(m_listeners.m_t.begin(), m_listeners.m_t.end(), pListener),
      m_listeners.m_t.end());
}

namespace {
std::vector<std::unique_ptr<const bms::IEvent>> take(
    bms::Lockable<std::vector<std::unique_ptr<const bms::IEvent>>> &events) {
  std::unique_lock lock(events.m_mutex);
  std::vector<std::unique_ptr<const bms::IEvent>> res;
  res.swap(events.m_t);
  return res;
}
}  // namespace

void bms::EventBus::run() {
  while (true) {
    std::unique_lock lock(m_cv.m_mutex);
    m_cv.m_t.wait(lock, [&]() { return hasWork(); });

    if (m_stop) {
      return;
    }

    publishToListeners(take(m_eventQueue));
  }
}
bool bms::EventBus::hasWork() const {
  std::unique_lock lock(m_eventQueue.m_mutex);
  return !m_eventQueue.m_t.empty() || m_stop;
}
void bms::EventBus::publishToListeners(
    std::vector<std::unique_ptr<const IEvent>> &&events) {
  std::unique_lock lock(m_listeners.m_mutex);
  for (auto pListener : m_listeners.m_t) {
    std::vector<std::unique_ptr<const IEvent>> clones;
    std::transform(events.begin(), events.end(), std::back_inserter(clones),
                   [](const std::unique_ptr<const IEvent> &upEvent) {
                     return std::unique_ptr<const IEvent>(upEvent->clone());
                   });
    pListener->fromBus(std::move(clones));
  }
}
