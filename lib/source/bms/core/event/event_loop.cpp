#include "bms/core/event/event_loop.h"

#include <algorithm>

#include "bms/core/event/ievent_bus.h"
#include "bms/core/event/ievent_handler.h"
#include "bms/core/time/timer.h"

bms::EventLoop::EventLoop(std::shared_ptr<IEventBus> spEventBus)
    : m_spEventBus(spEventBus),
      m_mutex(),
      m_cv(),
      m_stop(false),
      m_eventQueue(),
      m_inbox(),
      m_outbox(),
      m_eventHandlers(),
      m_next(m_eventHandlers.end()),
      m_thread([this]() { run(); }),
      m_upTimer() {
  m_upTimer = std::make_unique<Timer>(*this);
  m_spEventBus->add(this);
}
bms::EventLoop::~EventLoop() {
  m_upTimer.reset();
  m_spEventBus->remove(this);
  {
    std::unique_lock lock(m_mutex);
    m_stop = true;
  }
  m_cv.notify_one();
  m_thread.join();
}

// IEventLoop
void bms::EventLoop::dispatch(std::unique_ptr<const IEvent> upEvent) {
  {
    std::unique_lock lock(m_mutex);
    m_eventQueue.push_back(std::move(upEvent));
  }
  m_cv.notify_one();
}
void bms::EventLoop::publish(std::unique_ptr<const IEvent> upEvent) {
  {
    std::unique_lock lock(m_mutex);
    m_outbox.push_back(std::move(upEvent));
  }
  m_cv.notify_one();
}
void bms::EventLoop::add(IEventHandler* pEventHandler) {
  std::unique_lock lock(m_mutex);
  m_eventHandlers.push_back(pEventHandler);
}
void bms::EventLoop::remove(IEventHandler* pEventHandler) {
  std::unique_lock lock(m_mutex);
  auto itr = m_eventHandlers.end();
  while ((itr = std::find(m_eventHandlers.begin(), m_eventHandlers.end(),
                          pEventHandler)) != m_eventHandlers.end()) {
    m_next = m_eventHandlers.erase(itr);
  }
}

bms::ITimer& bms::EventLoop::timer() const { return *m_upTimer; }

// IEventBusListener
void bms::EventLoop::fromBus(
    std::vector<std::unique_ptr<const IEvent>> events) {
  // Perhaps this needs to be changed later; right now
  // it will block the bus until the current event loop update is
  // completed. Maybe that is not a problem but...
  {
    std::unique_lock lock(m_mutex);
    m_inbox.insert(m_inbox.end(), std::make_move_iterator(events.begin()),
                   std::make_move_iterator(events.end()));
  }
  m_cv.notify_one();
}

void bms::EventLoop::run() {
  while (true) {
    std::unique_lock lock(m_mutex);
    m_cv.wait(lock, [this]() { return hasWork(); });

    if (m_stop) {
      return;
    }

    // Take events from event queue
    std::vector<std::unique_ptr<const IEvent>> events;
    std::move(m_eventQueue.begin(), m_eventQueue.end(),
              std::back_inserter(events));
    m_eventQueue.clear();

    // Take events from event bus so that they are processed after the
    // internally queued events
    std::move(m_inbox.begin(), m_inbox.end(), std::back_inserter(events));
    m_inbox.clear();

    // Dispatch queued events
    pushToEventListeners(std::move(events));

    // Publish outgoing events
    m_spEventBus->push(std::move(m_outbox));
    m_outbox.clear();
  }
}
bool bms::EventLoop::hasWork() const {
  return !m_eventQueue.empty() || !m_inbox.empty() || !m_outbox.empty() ||
         m_stop;
}

void bms::EventLoop::pushToEventListeners(
    std::vector<std::unique_ptr<const IEvent>> events) {
  for (const auto& upEvent : events) {
    m_next = m_eventHandlers.begin();

    while (m_next != m_eventHandlers.end()) {
      (*m_next)->onEvent(*upEvent);
      m_next++;
    }
  }
}