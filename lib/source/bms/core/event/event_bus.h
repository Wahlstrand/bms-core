#pragma once

#include <atomic>
#include <condition_variable>
#include <thread>
#include <vector>

#include "bms/core/event/ievent_bus.h"
#include "bms/core/lockable.h"

namespace bms {
class EventBus final : public IEventBus {
 public:
  EventBus();
  ~EventBus();

  // IEventBus
  void push(std::vector<std::unique_ptr<const IEvent>> events) final;
  void add(IEventBusListener* pListener) final;
  void remove(IEventBusListener* pListener) final;

 private:
  Lockable<std::vector<IEventBusListener*>> m_listeners;
  Lockable<std::vector<std::unique_ptr<const IEvent>>> m_eventQueue;
  Lockable<std::condition_variable> m_cv;
  std::atomic<bool> m_stop;
  std::thread m_thread;

  void run();
  bool hasWork() const;
  void publishToListeners(std::vector<std::unique_ptr<const IEvent>>&& events);
};

}  // namespace bms