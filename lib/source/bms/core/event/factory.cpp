#include "bms/core/event/factory.h"

#include "bms/core/event/event_bus.h"
#include "bms/core/event/event_loop.h"

std::shared_ptr<bms::IEventBus> bms::createEventBus() {
  return std::make_shared<EventBus>();
}

std::unique_ptr<bms::IEventLoop> bms::createEventLoop(
    std::shared_ptr<IEventBus> spEventBus) {
  return std::make_unique<EventLoop>(spEventBus);
}