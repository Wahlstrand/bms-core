#pragma once

#include <atomic>
#include <condition_variable>
#include <list>
#include <mutex>
#include <thread>
#include <vector>

#include "bms/core/event/ievent_bus.h"
#include "bms/core/event/ievent_loop.h"
#include "bms/core/lockable.h"

namespace bms {
class EventLoop final : public IEventLoop, public IEventBusListener {
 public:
  EventLoop(std::shared_ptr<IEventBus> spEventBus);
  ~EventLoop();

  // IEventLoop
  void dispatch(std::unique_ptr<const IEvent> upEvent) final;
  void publish(std::unique_ptr<const IEvent> upEvent) final;
  ITimer& timer() const final;
  void add(IEventHandler* pEventHandler) final;
  void remove(IEventHandler* pEventHandler) final;

  // IEventBusListener
  void fromBus(std::vector<std::unique_ptr<const IEvent>> events) final;

 private:
  std::shared_ptr<IEventBus> m_spEventBus;
  std::recursive_mutex m_mutex;
  std::condition_variable_any m_cv;
  bool m_stop;
  std::vector<std::unique_ptr<const IEvent>> m_eventQueue;
  std::vector<std::unique_ptr<const IEvent>> m_inbox;
  std::vector<std::unique_ptr<const IEvent>> m_outbox;

  // These are a bit messy because we have to handle handlers being
  // added/removed during pushing of events
  std::list<IEventHandler*> m_eventHandlers;
  std::list<IEventHandler*>::iterator m_next;

  std::thread m_thread;
  std::unique_ptr<ITimer> m_upTimer;

  //
  void run();

  // These assume that lock is held
  bool hasWork() const;
  void pushToEventListeners(std::vector<std::unique_ptr<const IEvent>> events);
};

}  // namespace bms