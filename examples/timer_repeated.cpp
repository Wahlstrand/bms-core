#include <iostream>
#include <thread>

#include "bms/core/event/event.h"
#include "bms/core/event/event_handler.h"
#include "bms/core/event/factory.h"
#include "bms/core/event/ievent_loop.h"
#include "bms/core/time/itimer.h"

int main(int, char **) {
  // Create an event bus and an event loop
  auto spEventBus = bms::createEventBus();
  auto upEventLoop = bms::createEventLoop(spEventBus);

  // Define an event for this example
  using MyEvent = bms::Event<struct MyTag>;

  // Create an event handler for this event in the event loop
  bms::EventHandler<MyEvent> eventHandler(
      *upEventLoop, [](const MyEvent &) { std::cout << "Got MyEvent.\n"; });

  // Schedule every 400ms
  std::cout << "Schedule ever 400ms." << std::endl;
  using namespace std::chrono_literals;
  const uint64_t handle =
      upEventLoop->timer().repeat(std::make_unique<MyEvent>(), 400ms);

  // Main thread is free to do whatever. Here we just wait a while before
  // shutting down
  std::this_thread::sleep_for(3s);

  // Cancel events. Technically not required here because event loop is shutting
  // down anyway but cleaning up is a good habit.
  upEventLoop->timer().cancel(handle);

  // Done
  std::cout << "Done." << std::endl;
  return 0;
}