#include <iostream>
#include <thread>

#include "bms/core/event/event.h"
#include "bms/core/event/event_handler.h"
#include "bms/core/event/factory.h"
#include "bms/core/event/ievent_loop.h"

int main(int, char **) {
  // Create an event bus and and 2 event loops connected to this bus.
  auto spEventBus = bms::createEventBus();
  auto upEventLoop1 = bms::createEventLoop(spEventBus);
  auto upEventLoop2 = bms::createEventLoop(spEventBus);

  // Define an event for this example
  using MyEvent = bms::Event<struct MyTag>;

  // Create event handlers for this event, one in each event loop
  bms::EventHandler<MyEvent> eventHandler1(*upEventLoop1, [](const MyEvent &) {
    std::cout << "Handler in EventLoop1 got MyEvent.\n";
  });
  bms::EventHandler<MyEvent> eventHandler2(*upEventLoop2, [](const MyEvent &) {
    std::cout << "Handler in EventLoop2 got MyEvent.\n";
  });

  // Dispatch event on event loop 1 and wait a bit
  using namespace std::chrono_literals;
  std::cout << "\nDispatch MyEvent on event loop 1." << std::endl;
  upEventLoop1->dispatch(std::make_unique<MyEvent>());
  std::this_thread::sleep_for(1s);

  // Dispatch event on event loop 2 and wait a bit
  std::cout << "\nDispatch MyEvent on event loop 2." << std::endl;
  upEventLoop2->dispatch(std::make_unique<MyEvent>());
  std::this_thread::sleep_for(1s);

  // Publish event on event loop 1 and wait a bit
  std::cout << "\nPublish MyEvent on event loop 1." << std::endl;
  upEventLoop1->publish(std::make_unique<MyEvent>());
  std::this_thread::sleep_for(1s);

  // Publish event on event loop 2 and wait a bit
  std::cout << "\nPublish MyEvent on event loop 2." << std::endl;
  upEventLoop2->publish(std::make_unique<MyEvent>());
  std::this_thread::sleep_for(1s);

  // Done
  std::cout << "\nDone." << std::endl;
  return 0;
}