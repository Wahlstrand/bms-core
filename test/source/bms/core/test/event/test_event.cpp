#include <gmock/gmock.h>

#include "bms/core/event/event.h"

namespace bms {
namespace {

TEST(TestEvent, clone) {
  using MyEvent = bms::Event<struct MyTag, uint32_t>;
  uint32_t value = 315;
  MyEvent ev(value);
  std::unique_ptr<IEvent> upClone = ev.clone();
  const MyEvent* pTypedClone = dynamic_cast<MyEvent*>(upClone.get());
  ASSERT_FALSE(pTypedClone == nullptr);
  EXPECT_EQ(pTypedClone->m_val, value);
}

}  // namespace
}  // namespace bms