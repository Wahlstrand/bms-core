#include <gmock/gmock.h>

#include "bms/core/event/event.h"
#include "bms/core/event/event_handler.h"
#include "bms/core/test/event/mock_event_loop.h"

namespace bms {
namespace {

using EvGood = bms::Event<struct Good, uint32_t>;
using EvBad = bms::Event<struct Bad, uint32_t>;

TEST(TestEventHandler, onEventCorrectType) {
  testing::StrictMock<testing::MockFunction<void(const EvGood&)>> mockCallback;
  testing::NiceMock<MockEventLoop> mockEventLoop;
  EventHandler<EvGood> handler(mockEventLoop, mockCallback.AsStdFunction());

  uint32_t expectedVal = 32u;
  EXPECT_CALL(mockCallback, Call(testing::Field(&EvGood::m_val, expectedVal)));
  handler.onEvent(EvGood(expectedVal));
}

TEST(TestEventHandler, onEventWrongType) {
  testing::StrictMock<testing::MockFunction<void(const EvGood&)>> mockCallback;
  testing::NiceMock<MockEventLoop> mockEventLoop;
  EventHandler<EvGood> handler(mockEventLoop, mockCallback.AsStdFunction());

  EXPECT_CALL(mockCallback, Call(testing::_)).Times(0);
  handler.onEvent(EvBad(0u));
}

}  // namespace
}  // namespace bms