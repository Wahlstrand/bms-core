#include <gmock/gmock.h>

#include <future>

#include "bms/core/event/event.h"
#include "bms/core/event/factory.h"
#include "bms/core/test/event/mock_event_bus_listener.h"

namespace bms {
namespace {

TEST(TestEventBus, push) {
  using namespace std::chrono_literals;
  using MyEvent = bms::Event<struct MyTag>;
  auto upEventBus = createEventBus();

  std::promise<void> promise;
  testing::NiceMock<MockEventBusListener> mockListener;
  ON_CALL(mockListener, fromBus(testing::_))
      .WillByDefault(testing::Invoke(
          [&promise](std::vector<std::unique_ptr<const IEvent>> events) {
            if (events.size() != 1u) {
              return;
            }
            if (dynamic_cast<const MyEvent *>(events[0].get()) == nullptr) {
              return;
            }
            promise.set_value();
          }));

  upEventBus->add(&mockListener);

  std::vector<std::unique_ptr<const IEvent>> events;
  events.push_back(std::make_unique<const MyEvent>());
  upEventBus->push(std::move(events));
  EXPECT_TRUE(promise.get_future().wait_for(100ms) ==
              std::future_status::ready);

  upEventBus->remove(&mockListener);
}

}  // namespace
}  // namespace bms