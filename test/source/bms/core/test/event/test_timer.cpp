#include <gmock/gmock.h>

#include <future>

#include "bms/core/event/event.h"
#include "bms/core/test/event/mock_event_loop.h"
#include "bms/core/time/factory.h"

namespace bms {
namespace {
// Setting a tolerance here is a bit tricky. We want some accuracy but also
// don't want a flaky test.
TEST(TestTimer, single) {
  using namespace std::chrono_literals;
  using MyEvent = bms::Event<struct MyTag>;
  testing::NiceMock<MockEventLoop> mockEventLoop;
  auto upTimer = createTimer(mockEventLoop);

  const std::chrono::microseconds deltaUs = 30ms;
  const std::chrono::microseconds timeoutUs = 100ms;
  const std::chrono::milliseconds tolerance = 5ms;
  std::promise<std::chrono::time_point<std::chrono::steady_clock> >
      tReceivePromise;

  ON_CALL(mockEventLoop, dispatch(testing::_))
      .WillByDefault(testing::Invoke(
          [&tReceivePromise](std::unique_ptr<const IEvent> upEv) {
            const MyEvent* pEvent = dynamic_cast<const MyEvent*>(upEv.get());
            if (pEvent == nullptr) {
              return;
            }
            tReceivePromise.set_value(std::chrono::steady_clock::now());
          }));

  const std::chrono::time_point<std::chrono::steady_clock> tSchedule =
      std::chrono::steady_clock::now();
  upTimer->single(
      std::make_unique<MyEvent>(),
      std::chrono::duration_cast<std::chrono::milliseconds>(deltaUs));

  auto tReceiveFuture = tReceivePromise.get_future();
  ASSERT_TRUE(tReceiveFuture.wait_for(timeoutUs) == std::future_status::ready);
  const std::chrono::microseconds actualDeltaUs =
      std::chrono::duration_cast<std::chrono::microseconds>(
          tReceiveFuture.get() - tSchedule);
  EXPECT_LE(std::chrono::abs(actualDeltaUs - deltaUs), tolerance);
}

TEST(TestTimer, repeat) {
  using namespace std::chrono_literals;
  using MyEvent = bms::Event<struct MyTag>;
  testing::NiceMock<MockEventLoop> mockEventLoop;
  auto upTimer = createTimer(mockEventLoop);

  constexpr size_t N = 3;
  const std::chrono::microseconds deltaUs = 30ms;
  const std::chrono::microseconds timeoutUs = (N + 1) * deltaUs;
  const std::chrono::milliseconds tolerance = 5ms;

  using time_point = std::chrono::time_point<std::chrono::steady_clock>;
  std::array<std::promise<time_point>, N> tReceivePromises;

  // Reception times are ordered high->low because of how gtest handles multiple
  // expectations
  for (size_t n = 0; n < N; n++) {
    EXPECT_CALL(mockEventLoop, dispatch(testing::_))
        .WillOnce(testing::Invoke(
            [&tReceivePromises, n](std::unique_ptr<const IEvent> upEv) {
              const MyEvent* pEvent = dynamic_cast<const MyEvent*>(upEv.get());
              if (pEvent == nullptr) {
                return;
              }
              tReceivePromises[n].set_value(std::chrono::steady_clock::now());
            }))
        .RetiresOnSaturation();
  }
  const std::chrono::time_point<std::chrono::steady_clock> tSchedule =
      std::chrono::steady_clock::now();
  upTimer->repeat(
      std::make_unique<MyEvent>(),
      std::chrono::duration_cast<std::chrono::milliseconds>(deltaUs));

  for (size_t n = 0; n < tReceivePromises.size(); n++) {
    auto tReceiveFuture = tReceivePromises[n].get_future();
    ASSERT_TRUE(tReceiveFuture.wait_for(timeoutUs) ==
                std::future_status::ready);
    const std::chrono::microseconds actualDeltaUs =
        std::chrono::duration_cast<std::chrono::microseconds>(
            tReceiveFuture.get() - tSchedule);
    const std::chrono::microseconds expectedDeltaUs = (N - n) * deltaUs;
    EXPECT_LE(std::chrono::abs(actualDeltaUs - expectedDeltaUs), tolerance);
  }
}

TEST(TestTimer, cancel) {
  using namespace std::chrono_literals;
  using MyEvent = bms::Event<struct MyTag>;
  testing::NiceMock<MockEventLoop> mockEventLoop;
  auto upTimer = createTimer(mockEventLoop);

  const std::chrono::microseconds deltaUs = 30ms;
  const std::chrono::microseconds timeoutUs = 100ms;
  std::promise<std::chrono::time_point<std::chrono::steady_clock> >
      tReceivePromise;
  auto tReceiveFuture = tReceivePromise.get_future();

  ON_CALL(mockEventLoop, dispatch(testing::_))
      .WillByDefault(testing::Invoke(
          [&tReceivePromise](std::unique_ptr<const IEvent> upEv) {
            const MyEvent* pEvent = dynamic_cast<const MyEvent*>(upEv.get());
            if (pEvent == nullptr) {
              return;
            }
            tReceivePromise.set_value(std::chrono::steady_clock::now());
          }));

  auto handle = upTimer->single(
      std::make_unique<MyEvent>(),
      std::chrono::duration_cast<std::chrono::milliseconds>(deltaUs));
  upTimer->cancel(handle);

  EXPECT_TRUE(tReceiveFuture.wait_for(timeoutUs) ==
              std::future_status::timeout);
}

}  // namespace
}  // namespace bms