#include <gmock/gmock.h>

#include <future>

#include "bms/core/event/event.h"
#include "bms/core/event/factory.h"
#include "bms/core/test/event/mock_event_bus.h"
#include "bms/core/test/event/mock_event_handler.h"

namespace bms {
namespace {

TEST(TestEventLoop, dispatch) {
  using namespace std::chrono_literals;
  using MyEvent = bms::Event<struct MyTag, uint32_t>;
  testing::NiceMock<MockEventBus> mockEventBus;
  auto spEventBus = std::shared_ptr<IEventBus>(&mockEventBus, [](auto) {});
  auto upEventLoop = createEventLoop(spEventBus);

  const uint32_t val = 53;
  std::promise<void> promise;

  testing::NiceMock<MockEventHandler> mockEventHandler;
  ON_CALL(mockEventHandler, onEvent(testing::_))
      .WillByDefault(testing::Invoke([&promise](const IEvent& ev) {
        const MyEvent* pEvent = dynamic_cast<const MyEvent*>(&ev);
        if (pEvent != nullptr && pEvent->m_val == val) {
          promise.set_value();
        }
      }));
  upEventLoop->add(&mockEventHandler);
  upEventLoop->dispatch(std::make_unique<MyEvent>(val));

  EXPECT_TRUE(promise.get_future().wait_for(100ms) ==
              std::future_status::ready);

  upEventLoop->remove(&mockEventHandler);
}

TEST(TestEventLoop, publish) {
  using namespace std::chrono_literals;
  using MyEvent = bms::Event<struct MyTag, uint32_t>;
  testing::NiceMock<MockEventBus> mockEventBus;
  auto spEventBus = std::shared_ptr<IEventBus>(&mockEventBus, [](auto) {});
  auto upEventLoop = createEventLoop(spEventBus);

  const uint32_t val = 53;
  std::promise<void> promise;

  ON_CALL(
      mockEventBus,
      push(testing::ElementsAre(testing::An<std::unique_ptr<const IEvent>>())))
      .WillByDefault(testing::Invoke(
          [&promise](std::vector<std::unique_ptr<const IEvent>> events) {
            // We know from the matched that there is exactly one element
            auto pEvent = dynamic_cast<const MyEvent*>(events.begin()->get());
            if (pEvent != nullptr && pEvent->m_val == val) {
              {
                promise.set_value();
              }
            }
          }));

  upEventLoop->publish(std::make_unique<MyEvent>(val));

  EXPECT_TRUE(promise.get_future().wait_for(100ms) ==
              std::future_status::ready);
}

}  // namespace
}  // namespace bms