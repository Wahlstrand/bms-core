#pragma once

#include <gmock/gmock.h>

#include "bms/core/event/ievent_bus.h"

namespace bms {
class MockEventBus : public IEventBus {
 public:
  MOCK_METHOD1(push, void(std::vector<std::unique_ptr<const IEvent>>));
  MOCK_METHOD1(add, void(IEventBusListener*));
  MOCK_METHOD1(remove, void(IEventBusListener*));
};
}  // namespace bms
