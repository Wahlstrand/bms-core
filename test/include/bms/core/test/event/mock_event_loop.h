#pragma once

#include <gmock/gmock.h>

#include "bms/core/event/ievent_loop.h"

namespace bms {
class MockEventLoop : public IEventLoop {
 public:
  MOCK_METHOD1(dispatch, void(std::unique_ptr<const IEvent>));
  MOCK_METHOD1(publish, void(std::unique_ptr<const IEvent>));
  MOCK_CONST_METHOD0(timer, ITimer&());
  MOCK_METHOD1(add, void(IEventHandler*));
  MOCK_METHOD1(remove, void(IEventHandler*));
};
}  // namespace bms
