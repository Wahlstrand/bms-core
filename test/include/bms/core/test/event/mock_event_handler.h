#pragma once

#include <gmock/gmock.h>

#include "bms/core/event/ievent_handler.h"

namespace bms {
class MockEventHandler : public IEventHandler {
 public:
  MOCK_METHOD1(onEvent, void(IEvent const &ev));
};
}  // namespace bms
