#pragma once

#include <gmock/gmock.h>

#include "bms/core/event/ievent_bus.h"

namespace bms {
class MockEventBusListener : public IEventBusListener {
 public:
  MOCK_METHOD1(fromBus, void(std::vector<std::unique_ptr<const IEvent>>));
};
}  // namespace bms
